<?php
// Connection parameters
$host = 'xxx';
$dbname = 'xxx';
$username = 'xxx';
$password = 'xxx';

// Construct the PDO connection string
$dsn = "pgsql:host=$host;dbname=$dbname;user=$username;password=$password";

// Set additional options
$options = [
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_EMULATE_PREPARES => false,
    PDO::ATTR_PERSISTENT => true,
];

try {
    // Create a new PDO instance
    $pdo = new PDO($dsn, $username, $password, $options);
    
    echo 'seems fine';
} catch (PDOException $e) {
    // Handle connection errors
    echo 'Connection failed: ' . $e->getMessage();
    exit();
}
