<?php
ini_set('max_execution_time', '1200'); //1200 seconds = 20 minutes
$apiEndpoint = 'https://dataapi.uiscom.ru/v2.0';

$startDate = new DateTime('2022-10-01');
$endDate = (new DateTime('now'))->sub(new DateInterval('P1D'));
$currentDate = clone $startDate;
while ($currentDate <= $endDate) {
    $params = [
        'jsonrpc' => '2.0',
        'id' => 1,
        'method' => 'get.calls_report',
        'params' => [
            'date_from' => $currentDate->format('Y-m-d') . ' 00:00:00',
            'date_till' => $currentDate->format('Y-m-d') . ' 23:59:59',
            'access_token' => 'xxx',
            'fields' => ['id', 'direction', 'is_lost', 'start_time', 'contact_phone_number', 'employees'],
            'limit' => 10000,
        ],
    ];

    $curl = curl_init();

    curl_setopt_array($curl, [
        CURLOPT_URL => $apiEndpoint,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_POST => true,
        CURLOPT_POSTFIELDS => json_encode($params),
        CURLOPT_HTTPHEADER => [
            'Content-Type: application/json; charset=UTF-8',
        ],
    ]);

    $response = curl_exec($curl);

    if ($response === false) {
        echo 'cURL error: ' . curl_error($curl);
    } else {
        $responseData = json_decode($response, true);

        if ($responseData === null) {
            echo 'JSON decoding error: ' . json_last_error_msg();
        } else {
            print_r($responseData);
            $fileName = 'response_' . date('Y-m-d') . '.json';
            file_put_contents($fileName, $response);
        }
    }
    curl_close($curl);

    // fill db
    $host = 'localhost';
    $dbname = 'uis_db';
    $user = 'griborod';
    $password = '123';

    try {
        $pdo = new PDO("pgsql:host=$host;dbname=$dbname", $user, $password);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        foreach ($responseData['result']['data'] as $item) {
            $id = $item['id'];
            $isLost = $item['is_lost'] ? 'true' : 'false';
            $direction = $item['direction'];
            $employeeId = null;
            $contact_phone_number = $item['contact_phone_number'];

            if (!empty($item['employees'])) {
                $employeeId = $item['employees'][0]['employee_id'];
            }

            $startTime = $item['start_time'];

            $stmt = $pdo->prepare("INSERT INTO calls (id, is_lost, direction, employee_id, start_time, contact_phone_number) VALUES (?, ?, ?, ?, ?, ?)");
            $stmt->execute([$id, $isLost, $direction, $employeeId, $startTime, $contact_phone_number]);
        }

        echo "Data inserted successfully into PostgreSQL database.";

    } catch (PDOException $e) {
        die("Error: " . $e->getMessage());
    };
    $currentDate->modify('+1 day');
    usleep(100000);
}