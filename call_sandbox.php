<?php

// Database connection settings
$host = 'localhost';
$dbname = 'uis_db';
$user = 'user';
$password = 'ololo11';

// JSON file path
$jsonFile = 'D:\soft\OSPanel\domains\localhost\response_2024-02-09.json';

try {
    // Connect to PostgreSQL database
    $pdo = new PDO("pgsql:host=$host;dbname=$dbname", $user, $password);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    // Read JSON file
    $jsonData = file_get_contents($jsonFile);
    $data = json_decode($jsonData, true);

    // Extract data from JSON and insert into database
    foreach ($data['result']['data'] as $item) {
        $id = $item['id'];
        $isLost = $item['is_lost'] ? 'true' : 'false';
        $direction = $item['direction'];
        $employees = json_encode($item['employees']);
        $startTime = $item['start_time'];

        // Prepare INSERT statement
        $stmt = $pdo->prepare("INSERT INTO calls (id, is_lost, direction, employees, start_time) VALUES (?, ?, ?, ?, ?)");
        $stmt->execute([$id, $isLost, $direction, $employees, $startTime]);
    }

    echo "Data inserted successfully into PostgreSQL database.";

} catch (PDOException $e) {
    die("Error: " . $e->getMessage());
}
?>
