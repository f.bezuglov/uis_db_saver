<?php

$apiEndpoint = 'https://dataapi.uiscom.ru/v2.0';
$yesterday = date('Y-m-d', strtotime('-1 day'));
$params = [
    'jsonrpc' => '2.0',
    'id' => 1,
    'method' => 'get.calls_report',
    'params' => [
        'date_from' => $yesterday . ' 00:00:00',
        'date_till' => $yesterday . ' 23:59:59',
        'access_token' => 'xxx',
        'fields' => ['id', 'direction', 'is_lost', 'start_time', 'employees'],
        'limit' => 10000,
    ],
];

$curl = curl_init();

curl_setopt_array($curl, [
    CURLOPT_URL => $apiEndpoint,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_POST => true,
    CURLOPT_POSTFIELDS => json_encode($params),
    CURLOPT_HTTPHEADER => [
        'Content-Type: application/json; charset=UTF-8',
    ],
]);

$response = curl_exec($curl);

if ($response === false) {
    echo 'cURL error: ' . curl_error($curl);
} else {
    $responseData = json_decode($response, true);

    if ($responseData === null) {
        echo 'JSON decoding error: ' . json_last_error_msg();
    } else {
        print_r($responseData);
        $fileName = 'response_' . date('Y-m-d') . '.json';
        file_put_contents($fileName, $response);
    }
}
curl_close($curl);
