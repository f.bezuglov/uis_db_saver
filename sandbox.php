<?php

require_once 'D:\!scripts\uis\uis-app\Model.php';

// Read JSON file
$jsonFile = 'D:\!scripts\uis\uis-app\data.json';
$jsonData = file_get_contents($jsonFile);
$data = json_decode($jsonData, true);

if ($data === null) {
    die('Error decoding JSON file.');
}

// Initialize Model
$model = new Model();

// Fill employees table
if (isset($data['employees'])) {
    $employeesData = $data['employees'];
    $model->fillEmployeesTable($employeesData);
    echo "Employees table filled successfully.\n";
}

// Fill calls table
if (isset($data['calls'])) {
    $callsData = $data['calls'];
    $model->fillCallsTable($callsData);
    echo "Calls table filled successfully.\n";
}

echo "Data import completed.\n";
