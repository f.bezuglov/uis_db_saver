<?php


$apiEndpoint = 'https://dataapi.uiscom.ru/v2.0';
$params = [
    'jsonrpc' => '2.0',
    'id' => 1,
    'method' => 'get.calls_report',
    'params' => [
        'date_from' => "2024-01-01 00:00:00",
        'date_till' => "2024-02-10 20:00:00",
        'access_token' => 'xxx',
        'fields' => ['employees'],
    ],
];

$curl = curl_init();

curl_setopt_array($curl, [
    CURLOPT_URL => $apiEndpoint,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_POST => true,
    CURLOPT_POSTFIELDS => json_encode($params),
    CURLOPT_HTTPHEADER => [
        'Content-Type: application/json; charset=UTF-8',
    ],
]);

$response = curl_exec($curl);

if ($response === false) {
    echo 'cURL error: ' . curl_error($curl);
} else {
    $responseData = json_decode($response, true);

    if ($responseData === null) {
        echo 'JSON decoding error: ' . json_last_error_msg();
    } else {
        curl_close($curl);

        // choose unique pairs of ids and names
        if (isset($responseData['result']) && isset($responseData['result']['data'])) {
            // choose unique pairs of ids and names
            $uniqueEmployees = [];
    
            foreach ($responseData['result']['data'] as $item) {
                if (isset($item['employees']) && is_array($item['employees'])) {
                    foreach ($item['employees'] as $employee) {
                        $employeeId = $employee['employee_id'];
                        $employeeFullName = $employee['employee_full_name'];
                        
                        if (!isset($uniqueEmployees[$employeeId])) {
                            $uniqueEmployees[$employeeId] = $employeeFullName;
                        }
                    }
                }
            }
    
            print_r($uniqueEmployees);
        } else {
            echo "No 'result' or 'data' key found in the response.";
        }
    }
}


$host = 'xxx';
$dbname = 'xxx';
$user = 'xxx';
$password = 'xxx';

try {
    $pdo = new PDO("pgsql:host=$host;dbname=$dbname", $user, $password);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    foreach ($uniqueEmployees as $employeeId => $employeeName) {
        $stmt = $pdo->prepare("INSERT INTO employee_names (employee_id, employee_name) VALUES (?, ?)");
        $stmt->execute([$employeeId, $employeeName]);
    }

    echo "Data inserted successfully into PostgreSQL database.";

} catch (PDOException $e) {
    die("Error: " . $e->getMessage());
}