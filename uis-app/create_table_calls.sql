CREATE TABLE calls (
    id BIGINT PRIMARY KEY,
    is_lost BOOLEAN,
    direction VARCHAR(10),
    employee_id BIGINT,
    start_time TIMESTAMP,
	virtual_phone_number varchar(20),
	bitrix_client_id bigint
);
-- drop table calls