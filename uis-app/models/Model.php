<?php

namespace models;
require_once '../lib/Db.php';

use PDO;
use lib\Db;

class Model extends Db
{
    private $db;

    public function __construct()
    {
        $this->db = new Db();
    }
    public function fillEmployeesTable(array $uniqueEmployees): void
    {
        $tableName = 'employee_names';
        $fields = ['employee_id' => 'INTEGER', 'employee_name' => 'VARCHAR(255)'];
        $this->createTableAndInsertData($tableName, $fields, $uniqueEmployees);
    }

    public function fillCallsTable(array $callsData): void
    {
        $tableName = 'calls';
        $fields = ['id' => 'INTEGER', 'is_lost' => 'BOOLEAN', 'direction' => 'VARCHAR(255)', 'employee_id' => 'BIGINT', 'start_time' => 'TIMESTAMP', 'virtual_phone_number' => 'VARCHAR(20)', 'bitrix_client_id' => 'BIGINT'];
        $this->createTableAndInsertData($tableName, $fields, $callsData);
    }

    protected function createTableAndInsertData(string $tableName, array $fields, array $data): void
    {
        $this->createTableIfNotExists($tableName, $fields);
        $this->insertDataIntoTable($tableName, $fields, $data);
    }
    protected function tableExists(string $tableName): bool
    {
        $sql = "SELECT EXISTS (
                    SELECT 1
                    FROM information_schema.tables
                    WHERE table_name = :tableName
                )";
        $stmt = $this->execute($sql, [':tableName' => $tableName]);
        return (bool)$stmt->fetchColumn();
    }
    
    protected function createTableIfNotExists(string $tableName, array $fields): void
    {
        if (!$this->tableExists($tableName)) {
            $sql = "CREATE TABLE $tableName (id SERIAL PRIMARY KEY";
            foreach ($fields as $fieldName => $fieldType) {
                $sql .= ", $fieldName $fieldType";
            }
            $sql .= ")";
            $this->execute($sql);
        }
    }

    protected function insertDataIntoTable(string $tableName, array $fields, array $data): void
    {
        foreach ($data as $rowData) {
            $sql = "INSERT INTO $tableName (";
            $sql .= implode(', ', array_keys($fields)) . ') VALUES (';
            $placeholders = array_fill(0, count($fields), '?');
            $sql .= implode(', ', $placeholders) . ')';

            $params = [];
            foreach ($fields as $fieldName => $fieldType) {
                $params[] = $rowData[$fieldName] ?? null;
            }

            $this->execute($sql, $params);
        }
    }
}
