<?php

namespace models;

class UIS
{
    protected $apiEndpoint = 'https://dataapi.uiscom.ru/v2.0';
    protected $accessToken;

    public function __construct()
    {
        $this->accessToken = require_once(__DIR__ . '/../config/uis_token.php');
    }

    public function getCallsReport(string $dateFrom, string $dateTill): array
    {
        $params = [
            'jsonrpc' => '2.0',
            'id' => 1,
            'method' => 'get.calls_report',
            'params' => [
                'date_from' => $dateFrom,
                'date_till' => $dateTill,
                'access_token' => $this->accessToken,
                'fields' => ['id', 'direction', 'is_lost', 'start_time', 'employees'],
                'limit' => 10000,
            ],
        ];

        return $this->sendRequest($params);
    }

    public function getEmployees(string $dateFrom, string $dateTill): array
    {
        $params = [
            'jsonrpc' => '2.0',
            'id' => 1,
            'method' => 'get.calls_report',
            'params' => [
                'date_from' => $dateFrom,
                'date_till' => $dateTill,
                'access_token' => $this->accessToken,
                'fields' => ['employees'],
                'limit' => 10000,
            ],
        ];

        return $this->sendRequest($params);
    }

    public function getUniqueEmployeePairs(array $employeeArrays): array
    {
        $uniquePairs = [];

        foreach ($employeeArrays as $employeeArray) {
            if (isset($employeeArray['employees']) && is_array($employeeArray['employees'])) {
                foreach ($employeeArray['employees'] as $employee) {
                    $employeeId = $employee['employee_id'];
                    $employeeFullName = $employee['employee_full_name'];
                    if (!isset($uniquePairs[$employeeId])) {
                        $uniquePairs[$employeeId] = $employeeFullName;
                    }
                }
            }
        }

        return $uniquePairs;
    }

    protected function sendRequest(array $params): array
    {
        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => $this->apiEndpoint,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => json_encode($params),
            CURLOPT_HTTPHEADER => [
                'Content-Type: application/json; charset=UTF-8',
            ],
        ]);

        $response = curl_exec($curl);

        if ($response === false) {
            throw new \Exception('cURL error: ' . curl_error($curl));
        }

        $responseData = json_decode($response, true);

        if ($responseData === null) {
            throw new \Exception('JSON decoding error: ' . json_last_error_msg());
        }

        curl_close($curl);

        return $responseData;
    }
}
?>