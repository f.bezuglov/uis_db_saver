<?php

namespace lib;

use PDO;
use PDOException;

class Db
{
    protected static $PDO = null;
    public function __construct()
    {
        if (self::$PDO === null) {
            try {
                $config = require __DIR__ . '/../config/db.php';
                $dsn = "pgsql:host={$config['host']};port={$config['port']};dbname={$config['name']};keepalives_idle=60";
                self::$PDO = new PDO($dsn, $config['user'], $config['password']);
                self::$PDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            } catch (PDOException $e) {
                die("Connection failed: " . $e->getMessage());
            }
        }
    }

    public function execute($sql, $params = [])
    {
        try {
            $stmt = self::$PDO->prepare($sql);
            $stmt->execute($params);
            return $stmt;
        } catch (PDOException $e) {
            die("Query failed: " . $e->getMessage());
        }
    }

}
