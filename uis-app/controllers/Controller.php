<?php

require_once '../models/Model.php';

use models\Model;

// Read data from JSON file
$jsonData = file_get_contents('../data.json');
$data = json_decode($jsonData, true);

// Create an instance of the Model class
$model = new Model();

// // Fill the employees table
// if (isset($data['result']['data'])) {
//     $model->fillEmployeesTable($data['employees']);
// }

// Fill the calls table
if (isset($data['result']['data'])) {
    $model->fillCallsTable($data['result']['data']);
}

echo "Data imported successfully.";

?>
