<?php

$host = 'localhost';
$dbname = 'uis_db';
$user = 'user';
$password = 'ololo11';

$jsonFile = 'D:\soft\OSPanel\domains\localhost\response_2024-02-09.json';

try {
    $pdo = new PDO("pgsql:host=$host;dbname=$dbname", $user, $password);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $jsonData = file_get_contents($jsonFile);
    $data = json_decode($jsonData, true);

    foreach ($data['result']['data'] as $item) {
        $id = $item['id'];
        $isLost = $item['is_lost'] ? 'true' : 'false';
        $direction = $item['direction'];
        $employeeId = null;

        if (!empty($item['employees'])) {
            $employeeId = $item['employees'][0]['employee_id'];
        }

        $startTime = $item['start_time'];

        $stmt = $pdo->prepare("INSERT INTO calls (id, is_lost, direction, employee_id, start_time) VALUES (?, ?, ?, ?, ?)");
        $stmt->execute([$id, $isLost, $direction, $employeeId, $startTime]);
    }

    echo "Data inserted successfully into PostgreSQL database.";

} catch (PDOException $e) {
    die("Error: " . $e->getMessage());
}
