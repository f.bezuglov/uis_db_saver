<?php

$member = 'test';

ini_set('max_execution_time', '1200'); //1200 seconds = 20 minutes
ini_set('xdebug.var_display_max_depth', '-1');
ini_set('xdebug.var_display_max_children', '-1');
ini_set('xdebug.var_display_max_data', '-1');

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

ini_set('session.gc_maxlifetime', 2400);

spl_autoload_register(function ($class) {
    $path = __DIR__ . '/' . str_replace('\\', '/', $class . '.php');
    if (file_exists($path)) {
        require $path;
    }
});


$app = new Main(); // как в референсе передаётся мембер инфа?

try {
    $app->execute();

} catch (Exception $ex) {
    $message = "\n" . str_repeat('==', 30) . "\n";
    $message .= "\nКритическая ошибка во время выполнения\n";
    $message .= "Exception with code {$ex->getCode()}:\n\t{$ex->getMessage()}\n";
    $message .= "\nTrace:\n";
    $message .= $ex->getTraceAsString();
    echo $message;
}
