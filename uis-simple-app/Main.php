<?php

class Main {
    public $members;
    public $apiEndpoint = 'https://dataapi.uiscom.ru/v2.0';

    public function execute() {
        $this->logPath = dirname(__FILE__) . '/config/date.log';
        $this->dbPath = dirname(__FILE__) . '/config/db.php';
        if (file_exists(__DIR__ . '/config/members.php')) {
            $this->members = require __DIR__ . '/config/members.php';
        }

        if (!is_array($this->members)) {
            echo 'Файл members.php не найден или неверный формат';
            return;
        }
        
        $counter = 0; // костыль чтобы достать memberName
        foreach ($this->members as $key => $member) {
            $access_token = $member['access_token'];
            $member_id = $member['member_id'];

            $keys = array_keys($this->members); 
            $memberName = $keys[$counter]; 
            $counter = $counter + 1;
            
            $dateRanges = $this->date_ranges($this->count_date());
            foreach ($dateRanges as $dateRange) {
                $this->params_calls = [
                    'jsonrpc' => '2.0',
                    'id' => 1,
                    'method' => 'get.calls_report',
                    'params' => [
                        'date_from' => date('Y-m-d H:i:s', strtotime($dateRange[0] . '  00:00:00')),
                        'date_till' => date('Y-m-d H:i:s', strtotime($dateRange[1] . '  00:00:00')),
                        'access_token' => $access_token,
                        'fields' => ['id', 'direction', 'is_lost', 'start_time', 'employees', "virtual_phone_number", "contact_phone_number"],
                        'limit' => 10000,
                    ],
                ];

                $responseData = $this->sendRequest($this->params_calls);
                $numbers = $this->extractPhoneNumbers($responseData);
                $ids = $this->getBitrixClientIds($numbers, $member_id);
                $data = $this->addBitrixClientIds($responseData, $ids);
                $this->createCallsTable($this->dbPath, $memberName);
                $this->insertDataCalls($this->dbPath, $memberName, $data);
                $this->createTableEmployees($this->dbPath, $memberName);
                $this->insertDataEmployees($this->dbPath, $memberName, $data);
                file_put_contents($this->logPath, $dateRange[1]);
        }
        echo "Data inserted successfully to " . $memberName . '_calls' . "\n";
        echo "Data inserted successfully to " . $memberName . '_employees' . "\n";
        }
    }

    public function count_date() {// получить последнюю дату обновления данных и поставить вчерашнюю если данных нет
        $logPath = $this->logPath;
        if (file_exists($logPath)) {
            $lastDate = trim(file_get_contents($logPath));
            if ($lastDate === '') {
                $lastDate = date('Y-m-d', strtotime('-1 day'));
            }
        } else {
            $lastDate = date('Y-m-d', strtotime('-1 day'));
        }
    
        $yesterday = date('Y-m-d', strtotime('-1 day'));
            
        // file_put_contents($logPath, $yesterday); // не очень надёжно тут логировать 
        return $lastDate;
    }

    public function date_ranges($lastDate) { // вернет массив массивов с датами со сдвигом на 1 день с последней даты до вчера
        $yesterday = date('Y-m-d', strtotime('-1 day'));
        $currentDate = strtotime($lastDate);
        $dateRanges = [];
    
        while ($currentDate < strtotime($yesterday)) {
            $dateRanges[] = [
                date('Y-m-d', $currentDate), 
                date('Y-m-d', strtotime('+1 day', $currentDate)) 
            ];
            $currentDate = strtotime('+1 day', $currentDate);
        }
    
        return $dateRanges;
    }
    
    public function sendRequest($params)
    {
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL => 'https://dataapi.uiscom.ru/v2.0',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => json_encode($params),
            CURLOPT_HTTPHEADER => [
                'Content-Type: application/json; charset=UTF-8',
            ],
        ]);
        $response = curl_exec($curl);
    
        if ($response === false) {
            throw new \Exception('cURL error: ' . curl_error($curl));
        }

        $responseData = json_decode($response, true);
        if ($responseData === null) {
            throw new \Exception('JSON decoding error: ' . json_last_error_msg());
        }    
        
        curl_close($curl);
        return $responseData;
    }



    public function getBitrixClientId($phone, $member_id) { // для одного номера, route уже не работает
    
        $url = "https://app.bizzup.ru/mres/mis.php/get-patient-by-phone?member_id={$member_id}&phone={$phone}";
    
        $curl = curl_init($url);
    
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    
        $response = curl_exec($curl);
        curl_close($curl);
    
        $data = json_decode($response, true);
    
        if ($data['status'] === 'success') {
            return $data['data']['bitrix_id'];
        } else {
            return null; 
        }
    }

    public function getBitrixClientIds($phones, $member_id) { // для пачки номеров
        $baseUrl = "https://app.bizzup.ru/mres/mis.php/get-patient-by-phone";
        $clientIds = array();
        $chunks = array_chunk($phones, 50);
    
        foreach ($chunks as $chunk) {
            $url = $baseUrl . "?member_id={$member_id}";
            foreach ($chunk as $index => $phone) {
                $url .= "&phones[{$index}]={$phone}";
            }
    
            $curl = curl_init($url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($curl);
            curl_close($curl);
    
            $data = json_decode($response, true);
            if ($data['status'] === 'success' && $data['data'] !== null) {  // если пациент найден, добавить id в главный массив
                $clientIds = array_merge($clientIds, $data['data']);
            } else { // при ошибке добавить null, чтобы не нарушать индексы 
                $clientIds = array_merge($clientIds, array_fill(0, count($chunk), null));
            }
        }
    
        return $clientIds;
    }
    
    public function extractPhoneNumbers(array $responseData): array {
        $phoneNumbers = [];
        foreach ($responseData['result']['data'] as $entry) {
            if (!empty($entry['contact_phone_number'])) {
                $phoneNumbers[] = $entry['contact_phone_number'];
            } else {
                $phoneNumbers[] = null; // чтобы не нарушать индексы
            }
        }
        return $phoneNumbers;
    }

    public function addBitrixClientIds(array $responseData, array $ids): array { // добавить полученные id в массив данных из uis
        foreach ($responseData['result']['data'] as &$entry) {
            $bitrix_client_id = array_shift($ids); 
            
            $entry['bitrix_client_id'] = $bitrix_client_id;
        }
        unset($entry); 
        return $responseData;
    }
    
    
    public function createCallsTable($db_path, $member_name) {
        try {
            $credentials = require $db_path;
            $pdo = new PDO("pgsql:host={$credentials['host']};port={$credentials['port']};dbname={$credentials['name']}", $credentials['user'], $credentials['password']);
            $tableName = strtolower($member_name) . '_calls';
            $sql = "
            CREATE TABLE IF NOT EXISTS {$tableName} (
                id BIGINT PRIMARY KEY,
                is_lost BOOLEAN,
                direction VARCHAR(10),
                employee_id BIGINT,
                start_time TIMESTAMP,
                virtual_phone_number varchar(20),
                bitrix_client_id bigint
            )";
        
        $pdo->exec($sql);

        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
    }
    
    public function insertDataCalls($db_path, $member_name, $responseData) {
        try {
            $credentials = require $db_path;
            $pdo = new PDO("pgsql:host={$credentials['host']};port={$credentials['port']};dbname={$credentials['name']}", $credentials['user'], $credentials['password']);
            $tableName = strtolower($member_name) . '_calls';
    
            foreach ($responseData['result']['data'] as $item) {
                $id = $item['id'];
                $isLost = $item['is_lost'] ? 'true' : 'false';
                $direction = $item['direction'];
                $employeeId = null;
                $virtual_phone_number = $item['virtual_phone_number'];
                $bitrix_client_id = $item['bitrix_client_id'];
    
                if (!empty($item['employees'])) {
                    $employeeId = $item['employees'][0]['employee_id'];
                }
    
                $startTime = $item['start_time'];
    
                $stmt = $pdo->prepare("INSERT INTO {$tableName} (id, bitrix_client_id, virtual_phone_number, is_lost, direction, employee_id, start_time) VALUES (?, ?, ?, ?, ?, ?, ?)");
                $stmt->execute([$id, $bitrix_client_id, $virtual_phone_number, $isLost, $direction, $employeeId, $startTime]);
            }
            
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
    }

    public function createTableEmployees($db_path, $member_name) {
        try {
            $credentials = require $db_path;
            $pdo = new PDO("pgsql:host={$credentials['host']};port={$credentials['port']};dbname={$credentials['name']}", $credentials['user'], $credentials['password']);
            $tableName = strtolower($member_name) . '_employees';

            $sql = "
            CREATE TABLE IF NOT EXISTS {$tableName} (
                employee_id BIGINT PRIMARY KEY,
                employee_name VARCHAR(255)
            )";
        
        $pdo->exec($sql);
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
    }

    public function insertDataEmployees($db_path, $member_name, $responseData) {
        try {
            $credentials = require $db_path;
            $pdo = new PDO("pgsql:host={$credentials['host']};port={$credentials['port']};dbname={$credentials['name']}", $credentials['user'], $credentials['password']);
            $tableName = strtolower($member_name) . '_employees';
    
            foreach ($responseData['result']['data'] as $item) {
                $employeeId = null;
                $employeeName = null;
    
                if (!empty($item['employees'])) {
                    $employeeId = $item['employees'][0]['employee_id'];
                    $employeeName = $item['employees'][0]['employee_full_name'];
                }
        
                $stmt = $pdo->prepare("INSERT INTO {$tableName} (employee_id, employee_name) VALUES (?, ?)");
                $stmt->execute([$employeeId, $employeeName]);
            }
            
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
    }

}