<?php
ini_set('max_execution_time', '1200'); //1200 seconds = 20 minutes

$logPath = dirname(__FILE__) . '/date.log';

// read the last date and set it to yesterday if empty
if (file_exists($logPath)) {
    $lastDate = trim(file_get_contents($logPath));
    if ($lastDate === '') {
        $lastDate = date('Y-m-d', strtotime('-1 day'));
    }
} else {
    $lastDate = date('Y-m-d', strtotime('-1 day'));
}

$yesterday = date('Y-m-d', strtotime('-1 day'));
$tomorrow = date('Y-m-d', strtotime('+1 day'));
$dateFrom = date('Y-m-d H:i:s', strtotime($lastDate . '  12:00:00'));
$dateTill = date('Y-m-d H:i:s', strtotime($yesterday . '  13:00:00'));


$apiEndpoint = 'https://dataapi.uiscom.ru/v2.0';
$params = [
    'jsonrpc' => '2.0',
    'id' => 1,
    'method' => 'get.calls_report',
    'params' => [
        'date_from' => $dateFrom,
        'date_till' => $dateTill,
        'access_token' => 'xxx',
        'fields' => ['id', 'direction', 'is_lost', 'start_time', 'employees', "virtual_phone_number", "contact_phone_number"],
        'limit' => 10000,
    ],
];

$curl = curl_init();

curl_setopt_array($curl, [
    CURLOPT_URL => $apiEndpoint,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_POST => true,
    CURLOPT_POSTFIELDS => json_encode($params),
    CURLOPT_HTTPHEADER => [
        'Content-Type: application/json; charset=UTF-8',
    ],
]);

$response = curl_exec($curl);

if ($response === false) {
    echo 'cURL error: ' . curl_error($curl);
    curl_close($curl);
} else {
    $responseData = json_decode($response, true);
    curl_close($curl);
    if ($responseData === null) {
        echo 'JSON decoding error: ' . json_last_error_msg();
    } else {
        // print_r($responseData);
        $fileName = 'response_' . date('Y-m-d') . '.json';
        file_put_contents($fileName, json_encode($responseData));
    }
}
function getBitrixClientId($phone) {
    $memberId = '36a7d7473bcbe38e5409818268553a25';

    $url = "https://app.bizzup.ru/mres/mis.php/get-patient-by-phone?member_id={$memberId}&phone={$phone}";

    $curl = curl_init($url);

    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    $response = curl_exec($curl);
    curl_close($curl);

    $data = json_decode($response, true);

    // Check if patient found, return bitrix_id
    if ($data['status'] === 'success') {
        return $data['data']['bitrix_id'];
    } else {
        return null; // Or handle error accordingly
    }
}

// fill db
$host = 'localhost';
$dbname = 'uis_db';
$user = 'griborod';
$password = '123';

try {
    $pdo = new PDO("pgsql:host=$host;dbname=$dbname", $user, $password);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    foreach ($responseData['result']['data'] as $item) {
        $id = $item['id'];
        $isLost = $item['is_lost'] ? 'true' : 'false';
        $direction = $item['direction'];
        $employeeId = null;
        $virtual_phone_number = $item['virtual_phone_number'];
        $bitrix_client_id = getBitrixClientId($item['contact_phone_number']);
        // $item['bitrix_client_id'] = $bitrix_client_id;
        echo $item['contact_phone_number'];
        echo ": ";
        // $bitrix_client_id = $item['bitrix_client_id'];
        echo $bitrix_client_id;
        echo '  ';

        if (!empty($item['employees'])) {
            $employeeId = $item['employees'][0]['employee_id'];
        }

        $startTime = $item['start_time'];

        $stmt = $pdo->prepare("INSERT INTO calls (id, bitrix_client_id, virtual_phone_number, is_lost, direction, employee_id, start_time) VALUES (?, ?, ?, ?, ?, ?, ?)");
        $stmt->execute([$id, $bitrix_client_id, $virtual_phone_number, $isLost, $direction, $employeeId, $startTime]);
    }

    echo "Data inserted successfully into PostgreSQL database.";

} catch (PDOException $e) {
    die("Error: " . $e->getMessage());
}

// log the latest updated date
if (isset($responseData['result']) && !empty($responseData['result']['data'])) {
    file_put_contents($logPath, $yesterday);
}